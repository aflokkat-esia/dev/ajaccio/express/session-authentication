import { map } from "nanostores";

export type User = {
  id?: number;
  username?: string;
  password?: string;
  isAdmin?: boolean;
};

export const user = map<User>({});

export function setUser(userInfo: User) {
  user.set(userInfo);
}

export function removeUser() {
  user.set({});
}
