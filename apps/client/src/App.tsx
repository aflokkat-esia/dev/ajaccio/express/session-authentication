import { useStore } from "@nanostores/react";
import axios from "axios";
import { useEffect } from "react";
import { useCookies } from "react-cookie";
import { Route, Routes } from "react-router-dom";
import Home from "./pages/Home";
import Login from "./pages/Login";
import { setUser, user } from "./stores/user.store";

function App() {
  const [cookies] = useCookies(["sessionid"]);
  const authedUser = useStore(user);

  useEffect(() => {
    if (!authedUser?.username) {
      axios
        .post("http://localhost:3006/get-user-infos", {
          sessionId: cookies.sessionid,
        })
        .then((res) => setUser(res.data.user));
    }
  }, []);

  return (
    <>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/login" element={<Login />} />
      </Routes>
    </>
  );
}

export default App;
