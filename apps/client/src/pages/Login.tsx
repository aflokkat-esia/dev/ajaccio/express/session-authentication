import axios from "axios";
import { useRef } from "react";
import { useCookies } from "react-cookie";
import { useNavigate } from "react-router-dom";
import { setUser } from "../stores/user.store";

const Login = () => {
  const usernameRef = useRef<HTMLInputElement | null>(null);
  const passwordRef = useRef<HTMLInputElement | null>(null);

  const navigate = useNavigate();

  const [cookies, setCookie] = useCookies(["sessionid"]);

  function onClickSubmitForm() {
    axios
      .post("http://localhost:3006/login/", {
        username: usernameRef?.current?.value,
        password: passwordRef?.current?.value,
      })
      .then((res) => {
        setCookie("sessionid", res.data.sessionid);
        setUser(res.data.user);
        navigate("/");
      });
  }

  return (
    <>
      <div>Login</div>
      <input ref={usernameRef} type="text" placeholder="Username" />
      <input ref={passwordRef} type="text" placeholder="Password" />
      <button onClick={onClickSubmitForm}>Submit</button>
    </>
  );
};

export default Login;
