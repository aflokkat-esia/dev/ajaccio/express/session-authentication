import { useStore } from "@nanostores/react";
import axios from "axios";
import { useEffect, useState } from "react";
import { useCookies } from "react-cookie";
import { useNavigate } from "react-router-dom";
import { removeUser, user, User } from "../stores/user.store";
import { useAuth } from "../utils/CustomHooks";

const Home = () => {
  const navigate = useNavigate();
  const [cookies, , removeCookie] = useCookies(["sessionid"]);

  const [users, setUsers] = useState<User[]>([]);

  const authedUser = useStore(user);

  function onClickLogout() {
    axios
      .post("http://localhost:3006/logout/", {
        sessionId: cookies.sessionid,
      })
      .then(() => {
        removeCookie("sessionid");
        removeUser();
        navigate("/login");
      });
  }

  useEffect(() => {
    axios
      .get("http://localhost:3006/users", {
        headers: {
          sessionid: cookies.sessionid,
          isadmin: authedUser.isAdmin || false,
        },
      })
      .then((res) => setUsers(res.data));
  }, []);

  useAuth();

  return (
    <>
      {authedUser?.username ? (
        <>
          <div>Home</div>
          <p>Bienvenue, {authedUser?.username}</p>
          <button onClick={onClickLogout}>Déconnexion</button>
          {users && users.map((user) => <p key={user.id}>{user.username}</p>)}
        </>
      ) : (
        <p>Loading...</p>
      )}
    </>
  );
};

export default Home;
