import { NextFunction, Request, Response } from "express";
import { prismaClient } from "./prisma";

export async function checkAuth(
  req: Request,
  res: Response,
  next: NextFunction
) {
  if (req.headers.sessionid && typeof req.headers.sessionid === "string") {
    const session = await prismaClient.session.findFirst({
      where: {
        id: req.headers.sessionid,
      },
    });
    if (session) {
      // checkAdmin(req, res, next);
      next();
    } else {
      res.json({
        error: "Vous n'avez pas le droit d'accéder à cette information...",
      });
    }
  }
}

export async function checkAdmin(
  req: Request,
  res: Response,
  next: NextFunction
) {
  if (req.headers.isadmin && typeof req.headers.isadmin === "string") {
    if (req.headers.isadmin) {
      next();
    }
  } else {
    res.json({
      error: "Vous n'avez pas le droit d'accéder à cette information...",
    });
    return;
  }
}
