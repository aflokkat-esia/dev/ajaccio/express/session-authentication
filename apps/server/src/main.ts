import cors from "cors";
import dotenv from "dotenv";
import express from "express";
import { checkAuth } from "./middleware";
import { prismaClient } from "./prisma";

dotenv.config();

const app = express();

app.use(cors());
app.use(express.json());

app.get("/", (req, res) => {
  res.send("Hello, World!");
});

app.get("/users", checkAuth, async (req, res) => {
  const users = await prismaClient.user.findMany();
  res.json(users);
});

app.post("/login", async (req, res) => {
  const { username, password } = req.body;
  const user = await prismaClient.user.findFirst({
    where: {
      username,
    },
  });

  if (!user) {
    res.json({ error: "User doesn't exist..." });
    return;
  }

  if (user.password === password) {
    const session = await prismaClient.session.create({
      data: {
        userId: user.id,
      },
    });
    res.json({
      success: "Authentification réussie!",
      sessionid: session.id,
      user: user,
    });
  } else {
    res.json({ error: "Password doesn't match..." });
  }
});

app.post("/get-user-infos", async (req, res) => {
  const { sessionId } = req.body;
  const session = await prismaClient.session.findFirst({
    where: {
      id: sessionId,
    },
    include: {
      user: true,
    },
  });
  if (session) {
    res.json({ user: session.user });
    return;
  } else {
    res.json({ error: "Pas de session de disponible..." });
  }
});

app.post("/logout", async (req, res) => {
  const { sessionId } = req.body;
  await prismaClient.session.delete({
    where: {
      id: sessionId,
    },
  });
  res.json({ message: "Session supprimée!" });
});

app.listen(process.env.PORT, () => {
  console.log(`Server listening on port ${process.env.PORT}`);
});
